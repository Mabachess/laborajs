export default () => {
  return `<!doctype html>
    <html lang="fr">
      <head>
        <meta charset="utf-8">
        <title> LaboraCode </title>
      </head>
      <body>
        <div id="root"></div>
        <script type="text/javascript" src="/dist/bundle.js">
        </script>
      </body>
    </html>`
    
}
